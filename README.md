Forefront - Fedora Repository
==================================

This repository contains the latest released version of various packages.  It
aims to be completely compatible with the Fedora Production repositories - however
mpv is built based on the ffmpeg package in the Negativo17 Multimedia repository,
available here:  http://negativo17.org/handbrake/

If you use mpv, you must use the ffmpeg package from negativo17; otherwise it may
not work.

Command Line Setup using dnf:

su -c 'dnf -y install https://bitbucket.org/gbcox/forefront/raw/master/Fedora/25/noarch/fedora-forefront-1.0-3.fc25.noarch.rpm'

Additional information can be found in the repository wiki:
https://bitbucket.org/gbcox/forefront/wiki/Home

Licensed under GNU GPL version 3

Copyright © 2017 Gerald Cox (unless explicitly stated otherwise)