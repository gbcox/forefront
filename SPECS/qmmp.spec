Name:		qmmp
Version:	1.1.9
Release:	1%{?dist}
Summary:	Qt-based multimedia player

Group:		Applications/Multimedia
License:	GPLv2+ and CC-BY-SAv4+
URL:		http://qmmp.ylsoftware.com/
Source:		http://qmmp.ylsoftware.com/files/%{name}-%{version}.tar.bz2
Source2:	qmmp-filter-provides.sh
%define		_use_internal_dependency_generator 0
%define		__find_provides %{SOURCE2}

BuildRequires:	alsa-lib-devel
BuildRequires:	cmake
BuildRequires:	desktop-file-utils
BuildRequires:	enca-devel
BuildRequires:	flac-devel
BuildRequires:	game-music-emu-devel
BuildRequires:	jack-audio-connection-kit-devel
BuildRequires:	libbs2b-devel
BuildRequires:	libcddb-devel
BuildRequires:	libcdio-paranoia-devel
BuildRequires:	libcurl-devel
BuildRequires:	libmodplug-devel
BuildRequires:	libmpcdec-devel
BuildRequires:	libogg-devel
BuildRequires:	libprojectM-devel
BuildRequires:	libsamplerate-devel
BuildRequires:	libsidplayfp-devel
BuildRequires:	libsndfile-devel
BuildRequires:	libvorbis-devel
BuildRequires:	openssl-devel
BuildRequires:	opusfile-devel
BuildRequires:	pulseaudio-libs-devel
BuildRequires:	qt5-linguist
BuildRequires:	qt5-qtbase-devel
BuildRequires:	qt5-qtmultimedia-devel
BuildRequires:	qt5-qtx11extras-devel
BuildRequires:	soxr-devel
BuildRequires:	taglib-devel >= 1.10
BuildRequires:	wavpack-devel
BuildRequires:	wildmidi-devel

Requires(post):	/sbin/ldconfig
Requires(pre):	/sbin/ldconfig

%package devel
Summary:	Development files for qmmp
Group:		Development/Libraries
Requires:	%{name}%{?_isa} = %{version}-%{release}

%description
This program is an audio-player, written with help of Qt library.
The user interface is similar to winamp or xmms.
Main opportunities:

	* Winamp and xmms skins support
	* plugins support
	* Ogg Vorbis support
	* native FLAC support
	* Musepack support
	* WavePack support
	* ModPlug support
	* PCM WAVE support
	* CD Audio support
	* CUE sheet support
	* ALSA sound output
	* JACK sound output
	* OSS sound output
	* PulseAudio output
	* Last.fm/Libre.fm scrobbler
	* D-Bus support
	* Spectrum Analyzer
	* projectM visualization
	* sample rate conversion
	* bs2b dsp effect
	* streaming support
	* removable device detection
	* MPRIS support
	* global hotkey support
	* lyrics support

%description devel
QMMP is Qt-based audio player. This package contains its development files.

%prep
%setup -q

%build
%cmake \
	-D USE_AAC:BOOL=FALSE \
	-D USE_FFMPEG:BOOL=FALSE \
	-D USE_MAD:BOOL=FALSE \
	-D USE_MMS:BOOL=FALSE \
	-D USE_MPLAYER:BOOL=FALSE \
	-D QMMP_DEFAULT_OUTPUT=pulse \
	-D CMAKE_INSTALL_PREFIX=%{_prefix} \
	-D LIB_DIR=%{_lib} \
	./
make %{?_smp_mflags} VERBOSE=1

%install
%make_install
# Install desktop file
desktop-file-install --delete-original \
%if (0%{?fedora} && 0%{?fedora} < 19) || (0%{?rhel} && 0%{?rhel} < 7)
	--vendor fedora \
%endif
	--dir %{buildroot}%{_datadir}/applications \
	%{buildroot}/%{_datadir}/applications/qmmp.desktop
# filter out unsupported formats from MimeType
sed -i -e "s#audio/x-ffmpeg-shorten;##" \
       -e "s#audio/mp3;##" \
       -e "s#audio/mpeg;##" \
       -e "s#audio/x-mp3;##" \
       -e "s#audio/x-mpeg;##" \
       -e "s#audio/x-ms-wma;##" \
    %{buildroot}/%{_datadir}/applications/%{name}.desktop
sed -i -e "s#audio/x-ffmpeg-shorten;##" \
       -e "s#audio/mp3;##" \
       -e "s#audio/mpeg;##" \
       -e "s#audio/x-mp3;##" \
       -e "s#audio/x-mpeg;##" \
       -e "s#audio/x-ms-wma;##" \
    %{buildroot}/%{_datadir}/applications/%{name}_enqueue.desktop
# new files since 0.3.0, using Vendor is deprecated, so just validate
desktop-file-validate %{buildroot}/%{_datadir}/applications/%{name}_dir.desktop
desktop-file-validate %{buildroot}/%{_datadir}/applications/%{name}_enqueue.desktop

%files
%doc AUTHORS ChangeLog ChangeLog.rus README README.RUS
%license COPYING COPYING.CC-by-sa_V4
%{_bindir}/qmmp
%{_libdir}/qmmp
%{_libdir}/libqmmp*.so.*
%if (0%{?fedora} && 0%{?fedora} < 19) || (0%{?rhel} && 0%{?rhel} < 7)
%{_datadir}/applications/fedora-%{name}.desktop
%else
%{_datadir}/applications/%{name}.desktop
%endif
%{_datadir}/applications/%{name}_dir.desktop
%{_datadir}/applications/%{name}_enqueue.desktop
%{_datadir}/icons/hicolor/
%{_datadir}/%{name}/

%files devel
%{_includedir}/*
%{_libdir}/pkgconfig/qmmp*
%{_libdir}/libqmmp*.so

%post
/sbin/ldconfig
%{_bindir}/update-desktop-database &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
	%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi
 
%postun
/sbin/ldconfig
%{_bindir}/update-desktop-database &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
	%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi


%changelog
* Sat Jun 03 2017 Gerald Cox <gbcox@fedoraproject.org> 1.1.9-1
- Upstream release

* Thu Mar 23 2017 Gerald Cox <gbcox@fedoraproject.org> 1.1.8-1
- https://bitbucket.org/gbcox/forefront/issues/31/

* Sat Feb 04 2017 Gerald Cox <gbcox@fedoraproject.org> 1.1.7-1
- https://bitbucket.org/gbcox/forefront/issues/29/

* Sat Dec 24 2016 Gerald Cox <gbcox@fedoraproject.org> 1.1.5-1
- https://bitbucket.org/gbcox/forefront/issues/23/

* Tue Oct 04 2016 Gerald Cox <gbcox@fedoraproject.org> 1.1.4-1
- https://bitbucket.org/gbcox/forefront/issues/14/

* Fri Aug 26 2016 Gerald Cox <gbcox@fedoraproject.org> 1.1.3-1
- https://bitbucket.org/gbcox/forefront/issues/9/

* Mon Jul 25 2016 Gerald Cox <gbcox@fedoraproject.org> 1.1.2-2
- Bug 866 patch

* Sat Jul 23 2016 Gerald Cox <gbcox@fedoraproject.org> 1.1.2-1
- Upstream Release

* Sat Jul 09 2016 Gerald Cox <gbcox@fedoraproject.org> 1.1.1-1
- Upstream Release

* Thu Jun 23 2016 Karel Volný <kvolny@redhat.com> 1.1.0-1
- new version 1.1.0 (#1348548)
- see the upstream changelog at http://qmmp.ylsoftware.com/
- adds soxr (SoX Resampler) effect plugin
- adds qt5-qtmultimedia output plugin

* Fri Jun 03 2016 Karel Volný <kvolny@redhat.com> 1.0.10-1
- new version 1.0.10 (#1341421)
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Mon May 02 2016 Karel Volný <kvolny@redhat.com> 1.0.9-1
- new version 1.0.9 (#1332176)
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Mon Apr 04 2016 Karel Volný <kvolny@redhat.com> 1.0.7-1
- new version 1.0.7 (#1323535)
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Tue Mar 22 2016 Karel Volný <kvolny@redhat.com> 1.0.6-3
- rebuilt for libprojectM update

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.6-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Thu Jan 28 2016 Karel Volný <kvolny@redhat.com> 1.0.6-1
- new version 1.0.6 (#1302491)
- see the upstream changelog at http://qmmp.ylsoftware.com/
 - fixed gme plugin build
 - added feature to change default output plugin
   (removed workaround in specfile)
- updated provides filtering
- fix desktop files - filter out mimetypes that are not supported by this build
- added license info for the "glare" skin

* Mon Jan 04 2016 Karel Volný <kvolny@redhat.com> 1.0.5-1
- new version 1.0.5 (#1295137)
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Fri Dec 25 2015 Karel Volný <kvolny@redhat.com> 1.0.4-1
- new version 1.0.4
- see the upstream changelog at http://qmmp.ylsoftware.com/
- change default output to PulseAudio instead of ALSA

* Tue Nov 24 2015 Karel Volný <kvolny@redhat.com> 1.0.2-1
- new version 1.0.2
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Thu Oct 22 2015 Karel Volný <kvolny@redhat.com> 1.0.1-1
- new version 1.0.1
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Wed Oct 07 2015 Karel Volný <kvolny@redhat.com> 1.0.0-1
- new version 1.0.0
- see the upstream changelog at http://qmmp.ylsoftware.com/
- uses Qt5

* Tue Sep 08 2015 Karel Volný <kvolny@redhat.com> 0.9.1-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/
- adds QSUI from plugin pack
- removed qmmp_cue.desktop, no longer handled separately
- run update-desktop-database in %%post* scriptlets (bug #1242974)

* Tue Aug 25 2015 Karel Volný <kvolny@redhat.com> 0.8.8-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Fri Jul 10 2015 Hans de Goede <hdegoede@redhat.com> 0.8.5-2
- Rebuilt for libsidplayfp soname bump
- Misc. specfile cleanups

* Wed Jun 24 2015 Karel Volný <kvolny@redhat.com> 0.8.5-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Mon May 04 2015 Karel Volný <kvolny@redhat.com> 0.8.4-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Sat May 02 2015 Kalev Lember <kalevlember@gmail.com> - 0.8.3-2
- Rebuilt for GCC 5 C++11 ABI change

* Tue Feb 03 2015 Karel Volný <kvolny@redhat.com> 0.8.3-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Tue Nov 11 2014 Adrian Reber <adrian@lisas.de> 0.8.1-3
- rebuild for new libcdio-0.93

* Sun Aug 17 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Thu Aug 14 2014 Karel Volný <kvolny@redhat.com> 0.8.1-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Mon Jul 14 2014 Karel Volný <kvolny@redhat.com> 0.8.0-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/
- added SID support via libsidplay
- cleanup of BuildRequires

* Mon Jun 09 2014 Karel Volný <kvolny@redhat.com> 0.7.7-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Sun Jun 08 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Fri Jan 17 2014 Karel Volný <kvolny@redhat.com> 0.7.4-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/
- should fix bug #1049267

* Tue Aug 27 2013 Karel Volný <kvolny@redhat.com> 0.7.2-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Jun 20 2013 Karel Volný <kvolny@redhat.com> 0.7.1-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Thu Apr 18 2013 Karel Volný <kvolny@redhat.com> 0.7.0-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/
- project URLs changed
- add Opus support
- use UDisks2 instead of UDisks

* Tue Apr 02 2013 Karel Volný <kvolny@redhat.com> 0.6.8-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/index_en.php

* Fri Feb 22 2013 Toshio Kuratomi <toshio@fedoraproject.org> - 0.6.6-2
- Remove --vendor from desktop-file-install for F19 https://fedorahosted.org/fesco/ticket/1077

* Tue Jan 29 2013 Karel Volný <kvolny@redhat.com> 0.6.6-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/index_en.php

* Mon Jan 07 2013 Adrian Reber <adrian@lisas.de> 0.6.5-2
- rebuild for new libcdio-0.90

* Tue Dec 11 2012 Karel Volný <kvolny@redhat.com> 0.6.5-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/index_en.php

* Tue Nov 06 2012 Karel Volný <kvolny@redhat.com> 0.6.4-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/index_en.php

* Thu Aug 16 2012 Karel Volný <kvolny@redhat.com> 0.6.3-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/index_en.php

* Tue Jul 31 2012 Karel Volný <kvolny@redhat.com> 0.6.2-2
- move the unversioned libraries symlinks to -devel subpackage

* Mon Jul 30 2012 Karel Volný <kvolny@redhat.com> 0.6.2-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/index_en.php

* Fri Jul 27 2012 Karel Volný <kvolny@redhat.com> 0.6.1-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/index_en.php

* Sat Jul 21 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Tue Jul 3 2012 Karel Volný <kvolny@redhat.com> 0.6.0-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/index_en.php
- new qmmp_dir.desktop file
- provide new pkgconfig files in -devel

* Mon Jun 18 2012 Karel Volný <kvolny@redhat.com> 0.5.6-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/index_en.php

* Thu Jun 7 2012 Karel Volný <kvolny@redhat.com> 0.5.5-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/index_en.php

* Thu Mar 1 2012 Karel Volný <kvolny@redhat.com> 0.5.4-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/index_en.php
- removed cmake include patch (accepted upstream)

* Tue Feb 28 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.3-2
- Rebuilt for c++ ABI breakage

* Mon Jan 23 2012 Karel Volný <kvolny@redhat.com> 0.5.3-1
- new version
- see upstream changelog at http://qmmp.ylsoftware.com/index_en.php
- patch missing cmake include (qmmp-0.5.3-CheckCXXSourceCompiles.patch)

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Sun Nov 20 2011 Adrian Reber <adrian@lisas.de> 0.5.2-2
- rebuild for new libcdio

* Tue Sep 06 2011 Karel Volný <kvolny@redhat.com> 0.5.2-1
- new version
- see upstream changelog at http://qmmp.ylsoftware.com/index_en.php

* Fri Jun 24 2011 Karel Volný <kvolny@redhat.com> 0.5.1-1
- new version
- lots of improvements, see http://qmmp.ylsoftware.com/index_en.php
- added MIDI support via wildmidi and game music support via game-music-emu

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Wed Dec 15 2010 Karel Volný <kvolny@redhat.com> 0.4.3-1
- new version
- adds dvd autodetection
- lot of fixes

* Mon Sep 13 2010 Karel Volný <kvolny@redhat.com> 0.4.2-1
- new version
- adds Japanese and Spanish translations
- lot of fixes

* Wed Jun 30 2010 Karel Volný <kvolny@redhat.com> 0.4.1-1
- new version
- adds Dutch translation
- lot of fixes

* Thu Jun 10 2010 Karel Volný <kvolny@redhat.com> 0.4.0-1
- new version
- core rewrites, lots of new plugins
- BuildRequires enca-devel, libcddb-devel

* Tue Jun  1 2010 Ville Skyttä <ville.skytta@iki.fi> - 0.3.4-2
- Rebuild.

* Mon Apr 19 2010 Karel Volný <kvolny@redhat.com> 0.3.4-1
- new version
- fixes desktop file (yum warning issue), some other fixes

* Fri Apr 09 2010 Karel Volný <kvolny@redhat.com> 0.3.3-1
- new version
- adds Hungarian translation, some fixes

* Fri Jan 22 2010 Adrian Reber <adrian@lisas.de> 0.3.2-3
- rebuild for new libcdio

* Thu Jan 21 2010 Karel Volný <kvolny@redhat.com> 0.3.2-2
- rebuild for new libprojectM

* Wed Jan 13 2010 Karel Volný <kvolny@redhat.com> 0.3.2-1
- new version
- projectM 2.0 compatible (WRT bug #551855)

* Wed Nov 25 2009 Kevin Kofler <Kevin@tigcc.ticalc.org> 0.3.1-2
- rebuild for Qt 4.6.0 RC1 in F13 (was built against Beta 1 with unstable ABI)

* Wed Nov 04 2009 Karel Volný <kvolny@redhat.com> 0.3.1-1
- new version

* Wed Sep 02 2009 Karel Volný <kvolny@redhat.com> 0.3.0-3
- add libbs2b support, as it got added to Fedora (see bug #519138)

* Thu Aug 27 2009 Tomas Mraz <tmraz@redhat.com> - 0.3.0-2
- rebuilt with new openssl

* Tue Aug 25 2009 Karel Volný <kvolny@redhat.com> - 0.3.0-1
- new version
- updated %%description to match upstream
- new plugins = new BuildRequires, new .desktop files
- AAC support disabled due to patent restrictions
- mplayer plugin disabled due to mplayer missing from Fedora

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.3-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.3-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Feb 05 2009 Karel Volny <kvolny@redhat.com> 0.2.3-3
- do not own /usr/include in -devel subpackage (fixes bug #484098)

* Sat Jan 17 2009 Tomas Mraz <tmraz@redhat.com> 0.2.3-2
- rebuild with new openssl

* Fri Dec 05 2008 Karel Volny <kvolny@redhat.com> 0.2.3-1
- new version
- added %%{?_smp_mflags} to make, as parallel build was fixed

* Tue Sep 02 2008 Karel Volny <kvolny@redhat.com> 0.2.2-1
- new version

* Wed Jul 30 2008 Karel Volny <kvolny@redhat.com> 0.2.0-1
- new version
- updated %%description to match upstream
- added BuildRequires: libsndfile-devel wavpack-devel pulseaudio-libs-devel
- added BuildRequires: libmodplug-devel libcurl-devel openssl-devel
- xpm icon is not used anymore (several pngs available)
- created devel subpackage

* Mon May 19 2008 Karel Volny <kvolny@redhat.com> 0.1.6-2
- fixed %%description not to include patent-encumbered formats (bug #447141)

* Tue May 13 2008 Karel Volny <kvolny@redhat.com> 0.1.6-1
- new version

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.1.5-2
- Autorebuild for GCC 4.3

* Mon Dec 10 2007 Karel Volny <kvolny@redhat.com> 0.1.5-1
- new version
- simplified setting of library destination
- removed install-permissions patch, fixed upstream

* Wed Nov 21 2007 Karel Volny <kvolny@redhat.com> 0.1.4-5
- included Hans de Goede's patch for file permissions

* Mon Nov 19 2007 Karel Volny <kvolny@redhat.com> 0.1.4-4
- additional spec improvements as suggested in comment #10 to bug #280751

* Wed Sep 12 2007 Karel Volny <kvolny@redhat.com> 0.1.4-3
- additional spec improvements as suggested in comment #4 to bug #280751

* Tue Sep 11 2007 Karel Volny <kvolny@redhat.com> 0.1.4-2
- spec cleanup as suggested in comment #2 to bug #280751

* Mon Sep 10 2007 Karel Volny <kvolny@redhat.com> 0.1.4-1
- version bump
- install vendor-supplied .desktop file

* Thu Sep 6 2007 Karel Volny <kvolny@redhat.com> 0.1.3.1-2
- patched for multilib Fedora setup
- added .desktop entry and icon
- fixed spec to meet Fedora policies and rpm requirements
- removed ffmpeg and mad plugins to meet Fedora no-mp3 policy

* Wed Aug 1 2007 Eugene Pivnev <ti DOT eugene AT gmail DOT com> 1.1.9-1
- Initial release for Fedora 7
