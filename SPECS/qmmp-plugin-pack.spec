Name:           qmmp-plugin-pack
Version:        1.1.3
Release:        1%{?dist}
Summary:        A set of extra plugins for Qmmp

Group:          Applications/Multimedia
License:        GPLv2+
URL:            http://qmmp.ylsoftware.com/plugins.php
Source0:        http://qmmp.ylsoftware.com/files/plugins/%{name}-%{version}.tar.bz2

BuildRequires:  qmmp-devel >= 1.0.0
BuildRequires:  cmake
BuildRequires:  libsamplerate-devel
BuildRequires:  libxmp-devel
BuildRequires:  qt5-linguist
BuildRequires:  taglib-devel
BuildRequires:  yasm

%description
Plugin pack is a set of extra plugins for Qmmp.

 * FFap - enhanced Monkey's Audio (APE) decoder
   (24-bit samples and embedded cue support)
 * SRC - sample rate converter
 * XMP - module player with use of the libxmp library

%prep
%setup -q


%build
%cmake \
    -D USE_MPG123:BOOL=FALSE \
    .
make %{?_smp_mflags}


%install
%make_install


%files
%doc AUTHORS ChangeLog.rus README README.RUS
%license COPYING
%{_libdir}/qmmp/Effect/*.so
%{_libdir}/qmmp/Input/*.so


%changelog
* Sat Dec 24 2016 Gerald Cox <gbcox@fedoraproject.org> 1.1.3-1
- https://bitbucket.org/gbcox/forefront/issues/24/

* Sat Jul 09 2016 Gerald Cox <gbcox@fedoraproject.org> 1.1.1-1
- Upstream Release

* Thu Jun 23 2016 Karel Volný <kvolny@redhat.com> 1.1.0-1
- new version 1.1.0 (#1348549)
- see the upstream changelog at http://qmmp.ylsoftware.com/
- adds SRC (removed from qmmp)

* Fri Jun 03 2016 Karel Volný <kvolny@redhat.com> 1.0.4-1
- new version 1.0.4 (#1341422)
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Mon May 02 2016 Karel Volný <kvolny@redhat.com> 1.0.3-1
- new version 1.0.3 (#1332177)
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Tue Dec 29 2015 Karel Volný <kvolny@redhat.com> 1.0.2-1
- new version 1.0.2
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Mon Oct 12 2015 Karel Volný <kvolny@redhat.com> 1.0.1-1
- new version 1.0.1
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Wed Oct 07 2015 Karel Volný <kvolny@redhat.com> 1.0.0-1
- new version 1.0.0
- see the upstream changelog at http://qmmp.ylsoftware.com/
- uses Qt5

* Wed Sep 09 2015 Karel Volný <kvolny@redhat.com> 0.9.0-1
- new version 0.9.0
- see the upstream changelog at http://qmmp.ylsoftware.com/
- removes QSUI (added to qmmp)
- adds XMP

* Tue Aug 25 2015 Karel Volný <kvolny@redhat.com> 0.8.6-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/
- some specfile improvements

* Wed Jun 24 2015 Karel Volný <kvolny@redhat.com> 0.8.4-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.3-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sat May 02 2015 Kalev Lember <kalevlember@gmail.com> - 0.8.3-2
- Rebuilt for GCC 5 C++11 ABI change

* Tue Feb 03 2015 Karel Volný <kvolny@redhat.com> 0.8.3-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Sun Aug 17 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Thu Aug 14 2014 Karel Volný <kvolny@redhat.com> 0.8.1-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Tue Jul 15 2014 Karel Volný <kvolny@redhat.com> 0.8.0-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Mon Jun 09 2014 Karel Volný <kvolny@redhat.com> 0.7.7-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Sun Jun 08 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Fri Jan 17 2014 Karel Volný <kvolny@redhat.com> 0.7.4-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Wed Dec 11 2013 Karel Volný <kvolny@redhat.com> 0.7.3-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Tue Aug 27 2013 Karel Volný <kvolny@redhat.com> 0.7.2-1
- new version
- see the upstream changelog at http://qmmp.ylsoftware.com/

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Jun 20 2013 Karel Volný <kvolny@redhat.com> 0.7.1-1
- new version
- see upstream changelog at http://qmmp.ylsoftware.com/

* Sun Apr 28 2013 Karel Volný <kvolny@redhat.com> 0.7.0-1
- new version
- see upstream changelog at http://qmmp.ylsoftware.com/
- project URLs changes

* Tue Apr 02 2013 Karel Volný <kvolny@redhat.com> 0.6.6-1
- new version
- see upstream changelog at http://qmmp.ylsoftware.com/index_en.php

* Tue Jan 29 2013 Karel Volný <kvolny@redhat.com> 0.6.4-1
- new version
- see upstream changelog at http://qmmp.ylsoftware.com/index_en.php

* Tue Dec 11 2012 Karel Volný <kvolny@redhat.com> 0.6.3-1
- new version
- see upstream changelog at http://qmmp.ylsoftware.com/index_en.php

* Fri Aug 24 2012 Karel Volný <kvolny@redhat.com> 0.6.2-2
- update spec to newer style as suggested in package review
- removed %%buildroot actions
- removed %%clean section which got empty
- removed %%defattr

* Fri Aug 17 2012 Karel Volný <kvolny@redhat.com> 0.6.2-1
- new version
- fixes FSF address and execstack issues found by rpmlint
- see upstream changelog at http://qmmp.ylsoftware.com/index_en.php

* Tue Jul 31 2012 Karel Volný <kvolny@redhat.com> 0.6.1-1
- initial Fedora release
