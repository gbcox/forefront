%global	repo	forefront
%global	key	gbcox
%global	version	1.0
%global	release	3

Name:			fedora-%{repo}
Version:		%{version}
Release:		%{release}%{?dist}

Summary:		Bitbucket Fedora (%{repo}) Repository Configuration

Group:			System Environment/Base
License:		GPLv3
URL:			http://bitbucket.org/gbcox/forefront/
Source1:		fedora-%{repo}.repo
Source2:		RPM-GPG-KEY-%{key}

BuildArch:		noarch

Requires:		system-release >= %{version}


%description
This package contains the Forefront GPG key as well as DNF package manager
configuration files for the Fedora-Forefront repository, which is hosted
on Bitbucket.  This repository contains various cutting edge
packages which are not available in the Production Fedora repositories.

%prep
echo "Nothing to prep"

%build
echo "Nothing to build"

%install

# Create dirs
install -d -m755 \
  $RPM_BUILD_ROOT%{_sysconfdir}/pki/rpm-gpg  \
  $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d

# GPG Key
%{__install} -Dp -m644 \
    %{SOURCE2} \
    $RPM_BUILD_ROOT%{_sysconfdir}/pki/rpm-gpg

# compatibility symlink for easy transition to F11
ln -s $(basename %{SOURCE2}) $RPM_BUILD_ROOT%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-%{key}-fedora

# Avoid using basearch in name for the key. Introduced in F18
ln -s $(basename %{SOURCE2}) $RPM_BUILD_ROOT%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-%{key}-fedora-24

# Links for the keys
ln -s $(basename %{SOURCE2}) $RPM_BUILD_ROOT%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-%{key}-fedora-latest


# DNF .repo files
%{__install} -p -m644 %{SOURCE1} \
    $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d


%files
%{_sysconfdir}/pki/rpm-gpg/*
%config(noreplace) %{_sysconfdir}/yum.repos.d/*
%config(noreplace) %{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-%{key}

%changelog
* Wed Jan 18 2017 Gerald Cox <gbcox@fedoraproject.org> - 1.0-3
- https://bitbucket.org/gbcox/forefront/issues/26/

* Sun Jul 10 2016 Gerald Cox <gbcox@fedoraproject.org> - 1.0-2
- Converted to Bitbucket - Resolves Issue #2

* Fri Dec 5 2014 Gerald Cox <gbcox@fedora.org> - 1.0-1
- Initial RPM release
